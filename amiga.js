// Demo Amiga Old School (c) 2011
// 2015 - Revision 2.0 
// Developed by Carmelo Maiolino 
// This code is released under GPL licence


// Global Vars
var tavola_seno=new Array(50);
var tavola_seno_text= new Array(50);

// AMIGA fonts array
var fonts= new Array(5);

var amigaBall = new Array(15);
var amigaBallIndex=0;

// GODS Fonts ;)
var gFonts = new Array(38);

gFonts[0]= new Image();
gFonts[0].src="images/custom_fonts/_doublepoint.png";
gFonts[1]= new Image();
gFonts[1].src="images/custom_fonts/1.png";
gFonts[2]= new Image();
gFonts[2].src="images/custom_fonts/2.png";
gFonts[3]= new Image();
gFonts[3].src="images/custom_fonts/3.png";
gFonts[4]= new Image();
gFonts[4].src="images/custom_fonts/4.png";
gFonts[5]= new Image();
gFonts[5].src="images/custom_fonts/5.png";
gFonts[6]= new Image();
gFonts[6].src="images/custom_fonts/6.png";
gFonts[7]= new Image();
gFonts[7].src="images/custom_fonts/7.png";
gFonts[8]= new Image();
gFonts[8].src="images/custom_fonts/8.png";
gFonts[9]= new Image();
gFonts[9].src="images/custom_fonts/9.png";
gFonts[10]= new Image();
gFonts[10].src="images/custom_fonts/a.png";
gFonts[11]= new Image();
gFonts[11].src="images/custom_fonts/b.png";
gFonts[12]= new Image();
gFonts[12].src="images/custom_fonts/c.png";
gFonts[13]= new Image();
gFonts[13].src="images/custom_fonts/d.png";
gFonts[14]= new Image();
gFonts[14].src="images/custom_fonts/e.png";
gFonts[15]= new Image();
gFonts[15].src="images/custom_fonts/f.png";
gFonts[16]= new Image();
gFonts[16].src="images/custom_fonts/g.png";
gFonts[17]= new Image();
gFonts[17].src="images/custom_fonts/h.png";
gFonts[18]= new Image();
gFonts[18].src="images/custom_fonts/i.png";
gFonts[19]= new Image();
gFonts[19].src="images/custom_fonts/j.png";
gFonts[20]= new Image();
gFonts[20].src="images/custom_fonts/k.png";
gFonts[21]= new Image();
gFonts[21].src="images/custom_fonts/l.png";
gFonts[22]= new Image();
gFonts[22].src="images/custom_fonts/m.png";
gFonts[23]= new Image();
gFonts[23].src="images/custom_fonts/n.png";
gFonts[24]= new Image();
gFonts[24].src="images/custom_fonts/o.png";
gFonts[25]= new Image();
gFonts[25].src="images/custom_fonts/p.png";
gFonts[26]= new Image();
gFonts[26].src="images/custom_fonts/q.png";

gFonts[27]= new Image();
gFonts[27].src="images/custom_fonts/r.png";
gFonts[28]= new Image();
gFonts[28].src="images/custom_fonts/s.png";
gFonts[29]= new Image();
gFonts[29].src="images/custom_fonts/t.png";
gFonts[30]= new Image();
gFonts[30].src="images/custom_fonts/u.png";
gFonts[31]= new Image();
gFonts[31].src="images/custom_fonts/v.png";
gFonts[32]= new Image();
gFonts[32].src="images/custom_fonts/w.png";
gFonts[33]= new Image();
gFonts[33].src="images/custom_fonts/x.png";
gFonts[34]= new Image();
gFonts[34].src="images/custom_fonts/y.png";
gFonts[35]= new Image();
gFonts[35].src="images/custom_fonts/z.png";

gFonts[36]= new Image();
gFonts[36].src="images/custom_fonts/_point.png";
gFonts[37]= new Image();
gFonts[37].src="images/custom_fonts/0.png";


var fonts_values = new Array(38);

fonts_values[0]='..';
fonts_values[1]='1';
fonts_values[2]='2';
fonts_values[3]='3';
fonts_values[4]='4';
fonts_values[5]='5';
fonts_values[6]='6';
fonts_values[7]='7';
fonts_values[8]='8';
fonts_values[9]='9';
fonts_values[10]='a';

fonts_values[11]='b';
fonts_values[12]='c';
fonts_values[13]='d';
fonts_values[14]='e';
fonts_values[15]='f';
fonts_values[16]='g';
fonts_values[17]='h';
fonts_values[18]='i';
fonts_values[19]='j';
fonts_values[20]='k';
fonts_values[21]='l';
fonts_values[22]='m';
fonts_values[23]='n';
fonts_values[24]='o';
fonts_values[25]='p';
fonts_values[26]='q';
fonts_values[27]='r';
fonts_values[28]='s';
fonts_values[29]='t';
fonts_values[30]='u';
fonts_values[31]='v';
fonts_values[32]='w';
fonts_values[33]='x';
fonts_values[34]='y';
fonts_values[35]='z';
fonts_values[36]='.';
fonts_values[37]='0';



// Header image Commodore-Amiga
var logos= new Image();
logos.src="images/header_logo.png";


var text_scrolling = Array(10);
text_scrolling[0]="amiga 30th anniversary. 1985...2015. long life and prosperity ";
text_scrolling[1]="greetings to everyone from amiga html5 canvas demo";
text_scrolling[2]="a special tribute intro to the glorious commodore amiga times.";
text_scrolling[3]="developed by carmelo maiolino aka appnjoy. copyright 2015";
text_scrolling[4]="remeber... only amiga makes it possible ...";
text_scrolling[5]="drop me a line for feedback to my blog at cmaiolino.worpress.com";
text_scrolling[6]="and now... guru meditation";
text_scrolling[7]="guru meditation";
text_scrolling[8]="guru meditation";
text_scrolling[9]="if you didn't get the demo is over and you can come back with us to nowadays";

var text_index=0;
var current_text_length=0;
var FONT_BASE_WIDTH=16;
var delta=0;

var VIRTUAL_SCREEN_WIDTH=400;
var VIRTUAL_SCREEN_HEIGHT=280;



// Sin values for text scrolling
var SINMAX_TEXT_SCROLLING=50;
var n=0;
var ampiezza_text_scrolling=30;
var offset_text_Scrolling=3;


// Sin values for word AMIGA
var SINMAX=50;
var m=0;
var ampiezza=30;
var offset=3;


//////////////////////////////////
// circumference variables
var raggio=120;
var angolo = 0;
// Our circumference is divided in to 120 points
var totalNumbers= 120;
var nx;
var ny;
var circ=0;

////////////////////////////////
// Stars Variables
var pointsArray = [];
var numOfStars = 300;
var targetFps=60;

// Main Html 5 Canvas context
var contesto;




//Draw the Amiga rotating ball around a circumference
function draw_rotating_ball()
{
	var i;
	var x,y;
	
	angolo = ( 360 / totalNumbers ) * circ;
	nx = Math.sin( Math.PI / 180 * angolo ) * ( raggio  - 8 );
	ny = Math.cos( Math.PI / 180 * angolo ) * ( raggio  - 8 );
	x = ( nx + ( raggio ) );
	y = ( ny + ( raggio ) );
	circ++;
	if (circ>totalNumbers) circ=0;	
	contesto.fillStyle = 'white';
	
	amigaBallIndex++
	contesto.drawImage(amigaBall[amigaBallIndex%15],60+x,-5+y);

}


// x, y - Coords
// text - our text to draw
function draw_text(x, y ,text)
{
	var baseLine=0;

	for (i=0; i<text.length; i++)
	{
		
		var c = text.charAt(i);
		if (c==' ') 
		{
			// Whitespace 
			baseLine+=17;
		}
		
		if (idx = fonts_values.indexOf(c))
		{ 
			if (idx!=-1)
			{
				contesto.drawImage(gFonts[idx],x+baseLine,y+Math.round(tavola_seno_text[(m+offset*i)%49] ));
				
				baseLine+=17;
			}
		}
		
		
	}
	

}




// Init fonts, lookup table and stars 
function init()
{
  	var i;
	var angolo, angoloinc;
	angleinc = ( Math.PI  / (SINMAX) )*2;
	
	
	fonts[0] = new Image();
	fonts[0].src="images/a.png";
	
	fonts[1] = new Image();
	fonts[1].src="images/m.png";
	
	fonts[2] = new Image();
	fonts[2].src="images/i.png";
	
	fonts[3] = new Image();
	fonts[3].src="images/g.png";
	
		
	// Precarica l'animazione Ball Amiga
	for (i=0;i<15;i++)
	{
		amigaBall[i]=new Image();
		amigaBall[i].src="images/"+i+".png";
	}
	
	
	contesto = document.getElementById('mioCanvas').getContext('2d');

// Carichiamo l'array delle stelle 
	for (var i=0; i<numOfStars; ++i) {
		pointsArray[i] = new Star();
	}

   // Sin Scrolling for Amiga word
   for (i = 0, angolo = 0.0; i <= SINMAX; ++i, angolo += angleinc)
   {
      tavola_seno[i] = Math.round( (Math.sin(angolo)*ampiezza));
   }


 // Sin Scrolling for Amiga word
   for (i = 0, angolo = 0.0; i <= 180; ++i, angolo += angleinc)
   {
      tavola_seno_text[i] = Math.round( (Math.sin(angolo)*15));
   }

 
   // Calls the draw_canvas procedure every 16 milliseconds to reach 60 FPS
   setInterval(draw_canvas, 1000/targetFps);
   
}


// Stars animation
function loop() {

	 var context = document.getElementById('mioCanvas').getContext('2d');

	// the field
	for (var i=0; i<numOfStars; ++i) {
		var s = pointsArray[i];
		s.run();
		context.beginPath();
		context.fillStyle="#EFEFEF";
		context.rect(s.x,s.y,s.scale,s.scale);
		context.closePath();
		context.fill();
	}

	
}

// Stars coords, speed and zoom
function Star (){
	this.x = Math.random()*VIRTUAL_SCREEN_WIDTH;
	this.y = Math.random()*VIRTUAL_SCREEN_HEIGHT;

	this.scale = 1+Math.random()*2;
	this.speed = this.scale;

	this.run = function () {
		this.x -= this.speed;
		
		if (this.x < -10) {
			this.x = VIRTUAL_SCREEN_WIDTH+10;
			this.y = Math.random()*VIRTUAL_SCREEN_HEIGHT;
		}
	}
}



// The main drawing where all the magics happen :)
function draw_canvas()
{
	var contesto = document.getElementById('mioCanvas').getContext('2d');
	contesto.save();
    contesto.clearRect(0,0,VIRTUAL_SCREEN_WIDTH,VIRTUAL_SCREEN_HEIGHT);
    //ris=document.getElementById('message').innerHTML="AMIGA OLD SCHOOL demo by Carmelo Maiolino" ;

	// Disegna il gradiente
	draw_background();
	// Draw logo Commodore - Amiga
	contesto.drawImage(logos,-6,2);
	draw_rotating_ball();
	draw_amiga_word();
	loop();
	drawLine(0,40);
	drawLine(0,220);
	current_text_length=text_scrolling[text_index].length*17;
	
	
	delta=delta+2;
	if (VIRTUAL_SCREEN_WIDTH+current_text_length-delta<0) 
	{
		delta =0;
		text_index++;
	}
	if (text_index>9) text_index=0;
	draw_text(VIRTUAL_SCREEN_WIDTH-delta, 245,text_scrolling[text_index]);

	contesto.restore();
	
	
}

// Draw the background canvas
function draw_background()
{
	//Create gradient 
   var gradiente = contesto.createLinearGradient(0,80,0,VIRTUAL_SCREEN_HEIGHT);
	 gradiente.addColorStop(0.65, '#000000');
	gradiente.addColorStop(0.95, '#0d95c8');
 
 // Draw linear gradient
  contesto.fillStyle = gradiente;
  contesto.fillRect(0,80,VIRTUAL_SCREEN_WIDTH,VIRTUAL_SCREEN_HEIGHT);
  
  //Create gradient 
   var gradiente2 = contesto.createLinearGradient(0,0,0,60);
	 gradiente2.addColorStop(0.8, '#000000');
	 //gradiente2.addColorStop(0.15, '#0d95c8');
	 gradiente2.addColorStop(0.15, '#eeeeee');
 
 // Draw linear gradient
  contesto.fillStyle = gradiente2;
  contesto.fillRect(0,00,VIRTUAL_SCREEN_WIDTH,60);
  
  
 	
}


function draw_amiga_word()
{
	 // Lettera A
  contesto.drawImage(fonts[0],80,110+Math.round(tavola_seno[m] ));

// lettera M
  contesto.drawImage(fonts[1],130,110+Math.round(tavola_seno[(m+offset*2)%49] ));

// lettera I
  contesto.drawImage(fonts[2],180,110+Math.round(tavola_seno[(m+offset*3)%49] ));
  
  // lettera G
  contesto.drawImage(fonts[3],230,110+Math.round(tavola_seno[(m+offset*4)%49] ));

// lettera A
  contesto.drawImage(fonts[0],280,110+Math.round(tavola_seno[(m+offset*5)%49] ));
 
  m++;
  if (m>49) m=0;
   
	
}


// Draw a multicolored line 
function drawLine(x,y) {
		
		var gradient = contesto.createLinearGradient( 0,0,VIRTUAL_SCREEN_WIDTH, VIRTUAL_SCREEN_HEIGHT);
		
		for (var i=0; i < colorStops.length; i++) {
			var tempColorStop = colorStops[i];
			
			var tempColor = tempColorStop.color;
			var tempStopPercent = tempColorStop.stopPercent;
			gradient.addColorStop(tempStopPercent,tempColor);
			tempStopPercent += .015;
			if (tempStopPercent > 1) {
				tempStopPercent = 0;
			}
			tempColorStop.stopPercent = tempStopPercent;;
			colorStops[i] = tempColorStop;
		}

		contesto.beginPath();
		contesto.moveTo(x, y);
		contesto.lineTo(400, y+2);
		contesto.strokeStyle = gradient;
		contesto.stroke();
	
	
	}


var colorStops = new Array(
	{color:"#FF0000", stopPercent:0},
	{color:"#FFFF00", stopPercent:.125},
	{color:"#00FF00", stopPercent:.375},
	{color:"#0000FF", stopPercent:.625},
	{color:"#FF00FF", stopPercent:.875},
	{color:"#FF0000", stopPercent:1});




 
 
 
 
 
 
 
 
 
 
 
 
 
 
